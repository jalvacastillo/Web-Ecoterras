<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
// use Flash;

class HomeController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function index()
    {
        return view('index');
    }

    public function correo(Request $Request)
    {   

        try {
            Mail::send('emails.contacto', ['cliente' => $Request], function ($m) use ($Request) {
                $m->from('alvarado.websis@gmail.com', 'Jesús Alvarado')
                  ->to('alvarado.websis@gmail.com', 'Jesús Alvarado')
                  ->subject('Ecoterra');
            });
            
            $msj = 'Mensaje enviado';
            return view('index' , compact('msj'));

        } catch (Exception $e) {
            $msj = 'No se pudo enviar el Mensaje';
            return view('index' , compact('msj'));
        }

    }
    
}
