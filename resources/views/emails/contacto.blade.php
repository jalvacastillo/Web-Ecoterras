<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Contacto</title>
</head>
    <style>
        .container{
            width: 80%;
            margin: auto;
            font-family: helvetica, verdana, arial;
            color: #5C5656;
        }
    </style>
<body>
    
    <div class="container">
        <ul class="collection with-header">
            <h2>Saludos desde ECOTERRAS!</h2>
            
            <p>
            <strong>Te ha escrito:</strong> 
                {{ $cliente['nombre'] }}
            </p>
            <p>
            <strong>Su correo es: </strong>
                <a href="mailto:{{ $cliente['correo'] }}">{{ $cliente['correo'] }}</a>
            </p>
            <p>
            <strong>Esta solicitando lo siguiente:</strong>  <br>
                {{ $cliente['mensaje'] }}
            </p>
        </ul>
    </div>

</body>
</html>