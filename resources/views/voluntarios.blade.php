<div id="tf-testimonials" class="text-center">
    <div class="overlay">
        <div class="container">
            <div class="section-title center">
                <h2><strong>Programa</strong> Únete</h2>
                <div class="line">
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p>Forma parte de nuestro equipo en cualquiera de nuestros cuatro componentes.</p>
                    <br><br>
                </div>

                <div class="col-md-3">
                    <h3 style="color: #FCAC45;">HAZTE VOLUNTARIO</h3>
                    <p>Forma parte del equipo de voluntarios y tendrás la oportunidad de integrarte tanto en las actividades de fortalecimiento institucional necesarias para poner en marcha los diferentes programas y proyectos desarrollando</p>
                </div>
                <div class="col-md-3">
                    <h3 style="color: #FCAC45;">HASTE SOCIO</h3>
                    <p>Tanto personas como instituciones y organizaciones se pueden incorporar y  aportar recursos económicos, materiales y/o humanos para el mejoramiento de las condiciones de vida especialmente de sectores de la población más vulnerables. 
</p>
                </div>
                <div class="col-md-3">
                    <h3 style="color: #FCAC45;">RED DE COOPERACIÓN</h3>
                    <p> Profesionales, técnicos y personas con experiencia y conocimientos en materia de desarrollo en el trabajo que ECOTERRAS impulsa en los territorios pueden incorporarse activamente aportando tiempo y conocimientos.
                    </p>
                </div>
                <div class="col-md-3">
                    <h3 style="color: #FCAC45;">OFERTAS LABORALES</h3>
                    <p>
                        Se están definiendo perfiles y cargos laborales de los cuales puedes formar parte.
                    </p>
                </div>
                
                <div class="col-md-12">
                    <br><br><br>
                    <a href="{{ url('/docs/unete.pdf') }}" target="_black" class="btn tf-btn btn-default page-scroll">Conocer como unirte</a>
                </div>
            </div>
        </div>
    </div>
</div>