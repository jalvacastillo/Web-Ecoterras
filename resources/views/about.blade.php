<div id="tf-about">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="img/02.png" class="img-responsive" width="400" style="margin: 40px auto;">
            </div>
            <div class="col-md-7">
                <div class="about-text">
                    <div class="section-title">
                        <h2>Sobre <strong>nosotros</strong></h2>
                        <hr>
                        <br>
                    </div>

                    <div>

                      <ul class="about-list nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#nosotros" aria-controls="nosotros" role="tab" data-toggle="tab">Nosotros</a></li>
                        <li role="presentation"><a href="#mision" aria-controls="mision" role="tab" data-toggle="tab">Misión</a></li>
                        <li role="presentation"><a href="#vision" aria-controls="vision" role="tab" data-toggle="tab">Visión</a></li>
                        <li role="presentation"><a href="#valores" aria-controls="valores" role="tab" data-toggle="tab">Valores</a></li>
                        <li role="presentation"><a href="#docs" aria-controls="docs" role="tab" data-toggle="tab">Documentación</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content" style="padding: 15px;">
                        <div role="tabpanel" class="tab-pane active" id="nosotros">
                            <p class="intro">La Asociación nace con el propósito fundamental de trabajar activamente en la transformación de las causas que generan enormes brechas de desarrollo desigual entre diferentes sectores de la población salvadoreña mediante el desarrollo de procesos y acciones innovadoras que incidan en la construcción de un nuevo modelo de desarrollo democrático, sustentable y participativo, centrado en las familias bajo un enfoque generacional en los Territorios.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="mision">
                            <p class="intro">Una Comunidad Salvadoreña Sustentable cimentada en la participación activa de actores sociales comprometidos con la generación de cambios radicales en la construcción de un nuevo modelo de desarrollo inclusivo, democrático, participativo, y en comunión y armonía con el medio que la sustenta, centrado en las familias y comunidades como ejes del desarrollo en los Territorios.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="vision">
                            <p class="intro">Convertirnos en una institución sólida y sostenible que desarrolle y ponga en marcha mecanismos democráticos, participativos e inclusivos que a través de una dinámica transformadora e innovadora facilite y consolide procesos para la construcción de oportunidades y libertades económicas, sociales, culturales y la gestión sustentable de los territorios para el mejoramiento de las condiciones de vida de las familias en El Salvador.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="valores">
                            <p class="intro">
                            <span class="fa fa-dot-circle-o"></span> Solidaridad con los grupos vulnerables.<br>
                            <span class="fa fa-dot-circle-o"></span> Honestidad y honradez.<br>
                            <span class="fa fa-dot-circle-o"></span> Fomento de la Participación democrática. <br>
                            <span class="fa fa-dot-circle-o"></span> Fomento de la equidad e igualdad de oportunidades entre hombres y mujeres. <br>
                            <span class="fa fa-dot-circle-o"></span> Promoción y protección de los derechos humanos.<br>
                            <span class="fa fa-dot-circle-o"></span> Fomento de la educación y seguridad de las familias.
                            </p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="docs">
                            <p class="intro text-center">

                                <a href="{{ url('/docs/unete.pdf') }}" target="_black" class="btn btn-default page-scroll">Programa únete</a>
                                <a href="{{ url('/docs/territorios.pdf') }}" target="_black" class="btn btn-default page-scroll">Territorio de Incidencia</a>
                                <a href="{{ url('/docs/programas.pdf') }}" target="_black" class="btn btn-default page-scroll">Programas</a>

                            </p>
                        </div>
                        </div>
                    </div>                       
                </div>
            </div>
        </div>
    </div>
</div>