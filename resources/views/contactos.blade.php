<div id="tf-contact" class="text-center">
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="section-title center">
                    <h2>Puedes <strong>contactarnos</strong></h2>
                    <div class="line">
                        <hr>
                    </div>
                    <div class="clearfix"></div>
                    <p>Final 2ª Calle Poniente, Casa # 21, Barrio El Calvario, Municipio de Tejutepeque, Departamento de Cabañas</p>
                    <a href="tel:+5037051-5235" class="btn btn-default">(+503) 7051-5235</a>
                    <a href="tel:+5032389-0174" class="btn btn-default">(+503) 2389-0174</a>
                </div>

                <form method="post" action="/correo">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nombre:</label>
                                <input name="nombre" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre" required >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Correo:</label>
                                <input type="email" name="correo" class="form-control" id="exampleInputEmail1" placeholder="ejemplo@ejemplo.com" required >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mensaje:</label>
                        <textarea name="mensaje" class="form-control" rows="3" required placeholder="Tu solicitud"></textarea>
                    </div>
                    
                    <button type="submit" class="btn tf-btn btn-default">Enviar</button>
                </form>
            </div>
                @if (isset($msj))
                <div class="col-md-8 col-md-offset-2">
                <br>
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" role="alert" aria-hidden="true">&times;</button>
                        <strong>{{$msj}}</strong>
                    </div>
                </div>
                @endif
        </div>

    </div>
</div>