<div id="tf-works">
    <div class="container"> <!-- Container -->
        <div class="section-title text-center center">
            <h2>Nuestro <strong>trabajo</strong></h2>
            <div class="line">
                <hr>
            </div>
            <div class="clearfix"></div>
            <small>Conoce lo que estamos haciendo</small>
        </div>
        <div class="space"></div>

        <div class="categories">
            
            <ul class="cat">
                <li class="pull-left"><h4>Filtrar por:</h4></li>
                <li class="pull-right">
                    <ol class="type">
                        <li><a href="#" data-filter="*" class="active">Todos</a></li>
                        <li><a href="#" data-filter=".programas">Programas</a></li>
                        <li><a href="#" data-filter=".proyectos">Proyectos</a></li>
                    </ol>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div id="lightbox" class="row">

            <div class="col-sm-6 col-md-3 col-lg-3 programas">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Promoción y fortalecimiento de Redes de Actores para la Integración Productiva.</h4>
                                <small>Desarrollo Económico</small>
                                <p>Priorización y elaboración de perfiles de proyectos.
                                </p>
                            </div>
                            <img src="img/portfolio/01.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 proyectos">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Proyecto de semilla de Frijol</h4>
                                <small>Desarrollo Económico </small>
                                {{-- <p>Apoyando el cultivo con </p> --}}
                            </div>
                            <img src="img/portfolio/02.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 programas">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Servicios de desgranado de Maíz.</h4>
                                <small>Dinamización de las Economías Locales</small>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, asperiores?</p> --}}
                            </div>
                            <img src="img/portfolio/03.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 programas">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Comercialización de Productos Orgánicos en ferias</h4>
                                <small>Dinamización de las Economías Locales</small>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, officia!</p> --}}
                            </div>
                            <img src="img/portfolio/04.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 proyectos">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Promoción de Semilla Criolla y con mayor valor nutricional.</h4>
                                <small>Seguridad Alimentaria y Nutricional.</small>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, et.</p>
                            </div>
                            <img src="img/portfolio/05.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 programas">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Apoyo a Estudiantes Universitarios en la elaboración de Tesis</h4>
                                <small>Desarrollo social y Humano</small>
                                <p>Las Formas de participación ciudadana en la creación e implementación de Proyectos de desarrollo sostenible </p>
                            </div>
                            <img src="img/portfolio/06.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 programas">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Inclusión social e igualdad de oportunidades</h4>
                                <small>Desarrollo social y Humano</small>
                                <p>Familia de Productor de Granos Básicos. Proyectos de desarrollo con enfoque de ciclos de vida.</p>
                            </div>
                            <img src="img/portfolio/08.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 proyectos">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Formación en Gestión Empresarial para jóvenes</h4>
                                <small>Acción Social Territorial</small>
                                <p>Participación de un joven miembro de la iniciativa ADCUT – ECOTERRAS.  Como beneficiarios del Programa de formación (ADIT) y Programa Mundial de Alimentos. </p>
                            </div>
                            <img src="img/portfolio/07.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3 proyectos">
                <div class="portfolio-item">
                    <div class="hover-bg">
                        <a href="javascript:void(0)">
                            <div class="hover-text">
                                <h4>Gestión Sustentable de Recursos Naturales, Ecosistemas y Biodiversidad.</h4>
                                <small>Sustentabilidad Ambiental</small>
                                <p>Se esta determinanado el estado actual de conservación del recurso hídrico de los ríos Quezalapa y Aseseco.</p>
                            </div>
                            <img src="img/portfolio/11.jpg" class="" alt="...">
                        </a>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
</div>