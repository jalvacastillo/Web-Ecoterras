@extends('base')

@section('content')

    @include('menu')

    @include('banner')

    @include('about')
    
    @include('servicios')

    @include('voluntarios')

    @include('portafolio')

    @include('donaciones')

    @include('contactos')

    @include('footer')

@endsection