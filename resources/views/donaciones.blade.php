<div id="tf-clients" class="text-center">
    <div class="overlay">
        <div class="container">

            <div class="section-title center">
                <h2>Quieres <strong>Apoyarnos</strong></h2>
                <div class="line">
                    <hr>
                </div>
            </div>
            

            <h3>Puedes hacer donaciones</h3>
            <br>
            <p>Escribenos para conocer las formas en las que puedes donar y colabora con una buena obra</p>

            <a href="#tf-contact" class="btn tf-btn btn-default page-scroll">Escribenos</a>

        </div>
    </div>
</div>