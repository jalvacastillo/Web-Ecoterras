<nav id="tf-menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">ECOTERRAS</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#tf-home" class="page-scroll">Inicio</a></li>
        <li><a href="#tf-about" class="page-scroll">Quienes somos</a></li>
        <li><a href="#tf-services" class="page-scroll">Areas de Trabajo</a></li>
        {{-- <li><a href="#tf-services" class="page-scroll">Proyectos</a></li> --}}
        <li><a href="#tf-works" class="page-scroll">Proyectos</a></li>
        {{-- <li><a href="#tf-testimonials" class="page-scroll">Testimonials</a></li> --}}
        <li><a href="#tf-contact" class="page-scroll">Contactos</a></li>
      </ul>
    </div>
  </div>
</nav>