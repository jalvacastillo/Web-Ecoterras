<div id="tf-home" class="text-center">
    <div class="overlay">
        <div class="content">
            <h1>Somos <strong><span class="color">ECOTERRAS</span></strong></h1>
            <p class="lead">ASOCIACIÓN ECOSISTEMAS Y TERRITORIOS SUSTENTABLES </p>
            <a href="#tf-about" class="fa fa-angle-down page-scroll"></a>
        </div>
    </div>
</div>