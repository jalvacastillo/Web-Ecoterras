<div id="tf-services" class="text-center">
    <div class="container">
        <div class="section-title center">
            <h2>Areas de <strong>Trabajo</strong></h2>
            <div class="line">
                <hr>
            </div>
            <div class="clearfix"></div>
            <p>Como asociación trabajamos en las siguientes áreas estratégicas</p>
        </div>
        <div class="space"></div>
        <div class="row">
            <div class="col-md-6 col-sm-6 service">
                <i class="fa fa-money"></i>
                <h4><strong>DESARROLLO <br> ECONÓMICO</strong></h4>
                <p>Abonar a la generación de un entorno innovador en los territorios favorable para la integración productiva y la dinamización de las economías locales.</p>
                <p><b>Programas:</b></p>
                <ul class="list-group">
                  <li class="list-group-item">Promoción y fortalecimiento de Redes de Actores para la Integración Productiva.</li>
                  <li class="list-group-item">Dinamización de las Economías Locales.</li>
                  <li class="list-group-item">Seguridad Alimentaria y Nutricional.</li>
                </ul>
            </div>

            <div class="col-md-6 col-sm-6 service">
                <i class="fa fa-users"></i>
                <h4><strong>DESARROLLO SOCIAL <br> Y HUMANO</strong></h4>
                <p>Adoptar las relaciones y prácticas que desde la dimensión social conduzcan a un desarrollo más inclusivo, democrático y participativo.</p>
                <p><b>Programas:</b></p>
                <ul class="list-group">
                  <li class="list-group-item">Construcción y reconstrucción del tejido social.</li>
                  <li class="list-group-item">Inclusión social e igualdad de oportunidades.</li>
                  <li class="list-group-item">Acción Social Territorial.</li>
                </ul>
            </div>

            <div class="col-md-6 col-sm-12 service">
                <i class="fa fa-pagelines"></i>
                <h4><strong>SUSTENTABILIDAD <br> AMBIENTAL</strong></h4>
                <p>Desarrollar acciones que conduzcan a un manejo adecuado y correcto de los recursos naturales, ecosistemas y la biodiversidad para las presentes y futuras generaciones.</p>
                <p><b>Programas:</b></p>
                <ul class="list-group">
                  <li class="list-group-item">Gestión del riesgo y Cambio Climático.</li>
                  <li class="list-group-item">Gestión Sustentable de Recursos Naturales, Ecosistemas y Biodiversidad.</li>
                </ul>
            </div>
            
            <div class="col-md-6 col-sm-12 service">
                <i class="fa fa-binoculars"></i>
                <h4><strong>INVESTIGACIÓN e <br> INNOVACIÓN</strong></h4>
                <p>Potenciar el impulso a técnicas, conocimientos, recursos y estructuras, garantizando la incorporación de innovaciones tecnológicas y sociales en los procesos de desarrollo en los diferentes Territorios.</p>
                <p><b>Programas:</b></p>
                <ul class="list-group">
                  <li class="list-group-item">Sistema Territorial de Innovación (En construcción).</li>
                </ul>
            </div>

            <div class="col-md-12 col-sm-12 service" style="margin-bottom: 0; min-height: 0;">
                <i class="fa fa-comment"></i>
                <h4><strong>INFORMACIÓN y <br> COMUNICACIÓN</strong></h4>
                <p>Facilitar procesos de diálogo entre los diferentes actores de los territorios  encaminada a la transformación económica, social, cultural y política de los territorios y el mejoramiento de la calidad de vida de las personas.</p>
            </div>
            
            <div class="col-md-12">
                <br><br><br>
                <a href="{{ url('/docs/programas.pdf') }}" target="_black" class="btn tf-btn btn-default page-scroll">Conoce más acerca de nuestras areas de trabajo </a>
            </div>

        </div>
    </div>
</div>